package com.strongant.hadoop.client.demo.mr;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;


public class NumberDriver {

	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {


		final Configuration configuration = new Configuration();

		Job job = Job.getInstance(configuration, "NumberDriver");

		job.setJarByClass(NumberDriver.class);
		job.setReducerClass(NumberReducer.class);
		job.setMapperClass(NumberMapper.class);

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(LongWritable.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(LongWritable.class);


		FileInputFormat.setInputPaths(job,new Path(args[0]));
		FileOutputFormat.setOutputPath(job,new Path(args[1]));

		final boolean flag = job.waitForCompletion(true);
		System.exit(flag == true ? 0 : 1);


	}
}
