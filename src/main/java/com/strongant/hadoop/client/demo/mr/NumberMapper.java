package com.strongant.hadoop.client.demo.mr;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


public class NumberMapper extends Mapper<LongWritable, Text, LongWritable, LongWritable> {

	private LongWritable keyOut = new LongWritable();

	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		long outKey = Long.parseLong(value.toString());
		keyOut.set(outKey);

		context.write(keyOut, new LongWritable(1));
	}
}
