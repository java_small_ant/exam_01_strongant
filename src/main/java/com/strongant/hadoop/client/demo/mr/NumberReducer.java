package com.strongant.hadoop.client.demo.mr;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class NumberReducer extends Reducer<LongWritable, LongWritable, LongWritable, LongWritable> {

	private static LongWritable index = new LongWritable(1);

	@Override
	protected void reduce(LongWritable key, Iterable<LongWritable> values, Context context) throws IOException,
			InterruptedException {

		for (LongWritable val : values) {

			context.write(index, key);
			index = new LongWritable(index.get() + 1);
		}
	}
}
