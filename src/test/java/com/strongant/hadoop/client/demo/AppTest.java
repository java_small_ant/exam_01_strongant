package com.strongant.hadoop.client.demo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:wenhui.bai@baozun.com">wenhui.bai@baozun.com</a>
 * @since 2020/9/23
 */
public class AppTest {


	private static final String HDFS_URI = "hdfs://118.25.102.189:9000";
	private static final String USER_NAME = "root";
	Configuration configuration = null;
	FileSystem fileSystem = null;

	@Before
	public void before() throws URISyntaxException, IOException, InterruptedException {
		configuration = new Configuration();
		final URI url = new URI(HDFS_URI);
		final String userName = USER_NAME;
		fileSystem = FileSystem.get(url, configuration, userName);
	}


	@Test
	public void testHDFSClient() throws IOException {
		RemoteIterator<LocatedFileStatus> locatedFileStatusRemoteIterator = fileSystem.listFiles(new Path("/"), true);
		while (locatedFileStatusRemoteIterator.hasNext()) {
			LocatedFileStatus next = locatedFileStatusRemoteIterator.next();
			System.out.println(next.getPath());
		}
	}


	@Test
	public void uploadFileTest() throws IOException {
		fileSystem.copyFromLocalFile(new Path("/tmp/a.txt"),new Path("/wcinput"));
	}

	@Test
	public void downloadFileTest() throws IOException {
		fileSystem.copyToLocalFile(new Path("/wcinput/a.txt"), new Path("/tmp/b.txt"));
	}


	@Test
	public void removeFileTest() throws IOException {
		fileSystem.delete(new Path("/wcinput"), true);
	}

	@Test
	public void uploadFileInputStreamTest() throws IOException {
		FileInputStream fis = new FileInputStream(new File("/tmp/test.txt"));
		FSDataOutputStream fsDataOutputStream = fileSystem.create(new Path("/output"));
		IOUtils.copyBytes(fis, fsDataOutputStream, configuration);
	}


	@Test
	public void downloadFileByStreamTest() throws IOException {
		FSDataInputStream fis = fileSystem.open(new Path("/output"));
		FileOutputStream fsDataOutputStream = new FileOutputStream(new File("/tmp/c.txt"));
		IOUtils.copyBytes(fis, fsDataOutputStream, configuration);
	}


	/**
	 * seek 重复读取文件，指定任意位置读取文件
	 * @throws IOException
	 */
	@Test
	public void seekReadFileTest() throws IOException {
		FSDataInputStream fis = fileSystem.open(new Path("/output"));
		//FileOutputStream fsDataOutputStream = new FileOutputStream(new File("/tmp/c.txt"));
		IOUtils.copyBytes(fis, System.out, 4096,false);

		// 再次读取文件
		fis.seek(0);
		IOUtils.copyBytes(fis, System.out, 4096,false);

		IOUtils.closeStream(fis);
	}






	@After
	public void after() throws IOException {
		if (null != fileSystem) {
			fileSystem.close();
		}
	}

}